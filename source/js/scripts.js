$(document).ready(function(){

  $('.testimonials-slider-play-video').click(function (e) {
    e.preventDefault();
    $(this).siblings('video').get(0).play();
    var self = $(this);
    $(this).hide();
    $(this).siblings('video')[0].addEventListener('ended',function(){
      self.show();
    }, false);
  });

  var slider = $('.testimonials-slides');

  if (slider.length) {
    var currentSlide;
    var slidesCount;
    var sliderCounter= $('.testimonials-slider__counter');

    var updateSliderCounter = function(slick, currentIndex) {
      currentSlide = slick.slickCurrentSlide() + 1;
      slidesCount = slick.slideCount;
      $(sliderCounter).html(currentSlide + ' / <span>' +slidesCount+'</span>');
    };

    slider.on('init', function(event, slick) {
      updateSliderCounter(slick);
      $('.testimonials-slider__arrows-prev').hide();
    });

    slider.on('afterChange', function(event, slick, currentSlide) {
      if(currentSlide > 0){
        $('.testimonials-slider__arrows-prev').show();
      }else {
        $('.testimonials-slider__arrows-prev').hide();
      }
      updateSliderCounter(slick, currentSlide);
    });

    slider.slick({
      arrows: true,
      nextArrow: $('.testimonials-slider__arrows-next'),
      prevArrow: $('.testimonials-slider__arrows-prev'),
      infinite: false,
      variableWidth: true,
      speed: 250,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 812,
          settings: {
            variableWidth: false,
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
  }

});

