const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyPlugin = require('copy-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');

module.exports = {
  entry: {
    application: './source/index.js'
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'js/[name].[hash].js',
    publicPath: ''
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [{loader:'style-loader'}, {loader: MiniCssExtractPlugin.loader, options: {
          publicPath: '../'
        }}, {loader:'css-loader'}, {loader:'sass-loader'}]
      },
      {
        test: /\.(png|jpe?g|svg|gif)$/,
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]',
          context: 'source'
        }
      },
      {
        test: /\.(ttf|otf|woff|woff2)$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[path][name].[ext]',
            context: 'source'
          }
        }]
      },
      {
        test: /\.(mp4|webm)$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[path][name].[ext]',
            context: 'source'
          }
        }]
      },
      {
        test: /\.pug$/,
        use: ['pug-loader']
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'css/style.[hash].css'
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    }),
    new HtmlWebpackPlugin({
      inject: true,
      template: './source/html/index.pug',
      filename: 'index.html'
    }),
    new CopyPlugin([
      {
        from: 'source/libs',
        to: 'libs'
      }
    ]),
    new CleanWebpackPlugin()
  ]
};